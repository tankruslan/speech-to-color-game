import { handleResult } from './handlers';
import { colorsByLength, isDark } from './colors';

const colorsEl = document.querySelector('.colors');

window.SpeechRecognition =
  window.SpeechRecognition || window.webkitSpeechRecognition;

function displayColors(colors) {
  return colors
    .map(
      color =>
        `<span class="color ${
          isDark(color) ? 'dark' : ''
        } ${color}" style="background: ${color};">${color}</span>`
    )
    .join('');
}

function start() {
  if (!('SpeechRecognition' in window)) {
    console.log('Sorry your browser does not support speech recognition.');
    return;
  }
  const recognition = new SpeechRecognition();
  recognition.continuous = true;
  recognition.interimResults = true;
  recognition.onresult = handleResult;
  recognition.start();
}

start();
colorsEl.innerHTML = displayColors(colorsByLength);
